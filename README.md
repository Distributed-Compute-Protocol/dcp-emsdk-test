# DCP Emscripten Test
This repo demonstrates using Emscripten to deliver a C function for use in the worker.

## Usage
```
git clone git@gitlab.com:Distributed-Compute-Protocol/dcp-emsdk-test.git
cd dcp-emsdk-test
npm i
npm start
```
Visit `localhost:8080` in your browser (and open the dev console).
Take a look at `www/index.html` to set your config.


## How it works
There are 2 separate projects contained in this repo:
 1. The Worker Bundle: A javascript bundle containing the work function. There are 2 build steps involved in generating this:
    1. Use emsdk (Emscripten) to build the WASM and javascript wrapper from C code
    2. Use webpack to bundle the emsdk outputs with a wrapper containing the work function, which has access to functions provided by the Emscripten module
 2. The Client Page: A simple webpage + javascript bundle that uses dcp-client to submit a job that submits the worker bundle as the work function

### Repo Structure
`src/`: Source for the client webpage (submits the job using dcp-client)  
`worker-src/`: Source for the worker function bundle  
`www/`: Web root, contains bundled js output and html/css files  
`webpack.config.js`: Webpack config for the client page  
`worker.webpack.config.js`: Webpack config for the worker bundle  

## Building
### Worker Bundle
!! You need to have emscripten installed to build the worker bundle (at least the first part, emsdk output is provided so you don't need to rebuild it unless you're changing the C code)

```
cd worker-src
make # The makefile in this dir runs emsdk to produce a.out.js and a.out.wasm
cd ..
npm i
npm run build-worker
```

To use the worker bundle, run `npm start` to start the dev server and then visit `localhost:8080` in your browser.