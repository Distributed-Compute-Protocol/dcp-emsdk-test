/**
 * @author  Ryan Rossiter, ryan@kingsds.network
 * @date    Sep 2019
 * @file    index.js   
 *          This is the entry into the client bundle that
 *          submits the worker bundle as work to a job using dcp-client.
 */

// Runs when the "Go" button is clicked
const workerStart = async (workerBundle) => {
	job = dcp.compute.for([new Array(30).fill([2,2,3,4])], workerBundle);
	job.on('accepted', () => console.log("Job accepted"));
	job.on('result', (ev) => console.log(ev));
	let results = await job.exec(dcp.compute.marketValue)
    console.log(results);
}

const main = () => {
	fetch('js/worker-bundle.js')
  		.then(response => response.text())
  		.then((workerBundle) => {

  			// self.workerfn gets set by the workerBundle (see worker-src/index.js)
			const workerFn = `async (data) => {
				${workerBundle}
				return await self.workerfn(data);
			}`;

			console.log("Started main, waiting for go");
			document.getElementById('go-button').addEventListener('click', workerStart.bind(null, workerFn));
		});
}

document.addEventListener('DOMContentLoaded', main, false);
